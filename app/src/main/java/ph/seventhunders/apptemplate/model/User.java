package ph.seventhunders.apptemplate.model;

import android.arch.lifecycle.LiveData;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * Created by root on 2/28/18.
 */

@Entity
public class User {

    @Id long id;

    String FirstName, LastName;

    public User() {
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
