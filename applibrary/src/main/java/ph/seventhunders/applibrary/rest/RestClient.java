package ph.seventhunders.applibrary.rest;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import ph.seventhunders.applibrary.GeneralUtils;
import ph.seventhunders.applibrary.utils.Preferences;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Retrofit.Builder;

/**
 * Created by root on 2/28/18.
 */

public class RestClient {

    //private static ApiInterface mApiInterface;
    private static Retrofit mRetrofit = null;
    private static OkHttpClient client;

    public static <T> T create(final Context context, Class<T> interFace, final boolean withAuthorization) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        String baseUrl = GeneralUtils.getMetaDataValue(context, "base-url");
        if (baseUrl.length() == 0) {
            //KitchenUiUtils.showAlertDialog(context, "URL not found", "Base URL not found in manifest. Please declare a meta-data value with name \"base-url\".");
            return null;
        } else {
            final String authKey = Preferences.getString(context, Preferences.API_KEY);
            String fcmToken = Preferences.getString(context, "FCM_TOKEN");
            Log.d("authkey", "HELLO " + authKey);
            Log.d("authkey", "FCMKey " + fcmToken);
            PackageInfo pInfo = null;
            String appVersionName = "";

            try {
                pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                appVersionName = pInfo.versionName;
            } catch (Exception var12) {
                appVersionName = "N/A";
            }

            final String osVersion = Build.VERSION.RELEASE;
            Log.d("FB Token in Prefs", "" + Preferences.getString(context, Preferences.FCM_TOKEN));
            final String finalAppVersionName = appVersionName;
            client = (new okhttp3.OkHttpClient.Builder()).addInterceptor(interceptor).addInterceptor(new Interceptor() {
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder().addHeader("X-Authorization", authKey).addHeader("X-Device-App-Version", finalAppVersionName).addHeader("X-Device-OS", "android").addHeader("X-Device-OS-Version", osVersion).addHeader("X-Device-Manufacturer", "" + Build.MANUFACTURER).addHeader("X-Device-Model", "" + Build.MODEL).addHeader("X-Device-UDID", Settings.System.getString(context.getContentResolver(), "android_id")).addHeader("X-Country-ID", "13").addHeader("X-Device-FCM-Token", Preferences.getString(context, Preferences.FCM_TOKEN)).build();
                    return chain.proceed(request);
                }
            }).build();
            mRetrofit = (new Builder()).client(client).baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
            return mRetrofit.create(interFace);
        }
    }

    public static Retrofit getmRetrofit() {
        return mRetrofit;
    }

    public static OkHttpClient getClient() {
        return client;
    }
}
