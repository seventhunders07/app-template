package ph.seventhunders.apptemplate.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ph.seventhunders.applibrary.custom.BaseDashboardActivity;
import ph.seventhunders.applibrary.listeners.UICallback;
import ph.seventhunders.apptemplate.R;
import ph.seventhunders.apptemplate.model.User;
import ph.seventhunders.apptemplate.observers.MainActivityObserver;
import ph.seventhunders.apptemplate.viewmodel.MainViewModel;

public class MainActivity extends BaseDashboardActivity implements NavigationView.OnNavigationItemSelectedListener, UICallback {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.drawer)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    @BindView(R.id.user)
    AppCompatTextView mUserTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    @Override
    public int setLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public NavigationView setNavigationView() {
        return mNavigationView;
    }

    @Override
    public DrawerLayout setDrawerLayout() {
        return mDrawerLayout;
    }

    @Override
    public NavigationView.OnNavigationItemSelectedListener setListener() {
        return this;
    }

    @Override
    public ActionBarDrawerToggle setActionBarToggle() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.string_open_drawer, R.string.string_close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        return actionBarDrawerToggle;
    }

    @Override
    public int provideResMenu() {
        return R.menu.drawer_menu;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    private void initialize() {
        setToolbar(mToolbar);
        setToolbarTitle("Dashboard", false);
        MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // ArrayList<User> userData = viewModel.getAllUsers();
        // for (User user :
        //         userData) {
        //
        //
        // }
        viewModel.getUserLiveData().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                String strUsers = "";
                for (User userData : users) {
                    strUsers += userData.getFirstName() + " " + userData.getLastName() + "\n";
                }
                mUserTextView.setText(strUsers);
            }
        });
    }


    @Override
    public void setText(String text) {
        mUserTextView.setText(text);
    }
}
