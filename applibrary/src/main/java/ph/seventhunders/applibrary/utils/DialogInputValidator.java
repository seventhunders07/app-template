package ph.seventhunders.applibrary.utils;

import android.text.Editable;
import android.text.TextWatcher;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public class DialogInputValidator implements TextWatcher {

    MaterialDialog mMaterialDialog;
    boolean mIsText;

    public DialogInputValidator(MaterialDialog materialDialog, boolean isText) {
        mMaterialDialog = materialDialog;
        mIsText = isText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!mIsText) {
            if (s.toString().length() > 11) {
                mMaterialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                return;
            }
        }
        if (s.toString().isEmpty()) {
            mMaterialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
            return;
        }
        mMaterialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
    }
}