package ph.seventhunders.applibrary;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import ph.seventhunders.applibrary.utils.DialogInputValidator;

/**
 * Created by root on 2/28/18.
 */

public class UIUtils {

    private static AlertDialog alertDialog;

    public static View inflateViewFromResources(Context context, int layoutResourceId) {
        return LayoutInflater.from(context).inflate(layoutResourceId, null, false);
    }

    public static void showDialog(Context context, String title, String message, String negativeButtonTitle, String positiveButtonTitle, DialogInterface.OnClickListener onClickNegative, DialogInterface.OnClickListener onClickPositive) {
        if (alertDialog != null && alertDialog.isShowing()) return;
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(Html.fromHtml(message))
                .setCancelable(false)
                .setPositiveButton(positiveButtonTitle, onClickPositive)
                .setNegativeButton(negativeButtonTitle, onClickNegative).create();
        alertDialog.show();
    }

    public static void showDialog(Context context, String title, String message, String positiveButtonTitle, DialogInterface.OnClickListener onClickPositive) {
        if (alertDialog != null && alertDialog.isShowing()) return;
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(Html.fromHtml(message))
                .setCancelable(false)
                .setPositiveButton(positiveButtonTitle, onClickPositive)
                .create();
        alertDialog.show();
    }

    public void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showSingleInputDialog(Context context, String title, String positiveButtonText, MaterialDialog.SingleButtonCallback listener, String hintText, String input, boolean isText) {
        MaterialDialog materialDialog = new MaterialDialog.Builder(context)
                .title(title)
                .customView(R.layout.one_input_dialog_layout, true)
                .positiveText(positiveButtonText)
                .negativeText("Cancel")
                .onPositive(listener)
                .onNegative(null)
                .autoDismiss(true)
                .cancelable(false)
                .build();
        View view = materialDialog.getCustomView();
        MaterialEditText inputOne = view.findViewById(R.id.inputOneEditText);
        inputOne.setInputType(isText ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_NUMBER);
        inputOne.setHint(hintText);
        inputOne.setFloatingLabelText(hintText);
        inputOne.setText(input.isEmpty() ? "" : input);
        inputOne.setSelection(input.length());
        materialDialog.show();
        if (!isText) {
            inputOne.setMaxCharacters(11);
            inputOne.addTextChangedListener(new DialogInputValidator(materialDialog, isText));
            return;
        }
        inputOne.addTextChangedListener(new DialogInputValidator(materialDialog, isText));

    }
}
