package ph.seventhunders.applibrary.observers;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;

import ph.seventhunders.applibrary.listeners.UICallback;

/**
 * Created by root on 2/28/18.
 */

public abstract class BaseObserver implements LifecycleObserver {

    public abstract void BaseObserver(Context context, Lifecycle lifecycle, UICallback callback);

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public abstract void onViewCreated();

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public abstract void onViewStarted();

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public abstract void onViewStopped();

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public abstract void onViewResumed();

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public abstract void onViewDestroyed();

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public abstract void onViewPaused();
}
