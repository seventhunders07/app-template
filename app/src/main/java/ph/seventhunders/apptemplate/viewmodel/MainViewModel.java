package ph.seventhunders.apptemplate.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;

import io.objectbox.android.ObjectBoxLiveData;
import ph.seventhunders.apptemplate.database.BoxDao;
import ph.seventhunders.apptemplate.model.User;

/**
 * Created by root on 2/28/18.
 */

public class MainViewModel extends ViewModel {

    private ArrayList<User> mUserDatas;
    private BoxDao<User> mBoxDao;
    private ObjectBoxLiveData<User> mUserData;

    public void insertUser(String firstname, String lastName) {
        User user = new User();
        user.setFirstName(firstname);
        user.setLastName(lastName);
        getUserBoxDao().insertData(User.class, user);
    }

    public ArrayList<User> getAllUsers() {
        if(mUserDatas == null) {
            mUserDatas = getUserBoxDao().getAllData(User.class);
        }
        return mUserDatas;
    }

    public BoxDao<User> getUserBoxDao() {
        if (mBoxDao == null) {
            mBoxDao = new BoxDao<>();
        }
        return mBoxDao;
    }

    public ObjectBoxLiveData<User> getUserLiveData(){
        if(mUserData == null){
            mUserData = getUserBoxDao().getUserLiveData(User.class);
        }
        return mUserData;
    }
}
