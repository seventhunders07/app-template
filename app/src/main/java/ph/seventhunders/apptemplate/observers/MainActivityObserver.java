package ph.seventhunders.apptemplate.observers;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import ph.seventhunders.applibrary.UIUtils;
import ph.seventhunders.applibrary.listeners.UICallback;
import ph.seventhunders.applibrary.observers.BaseObserver;
import ph.seventhunders.apptemplate.database.BoxDao;
import ph.seventhunders.apptemplate.model.User;

/**
 * Created by root on 2/28/18.
 */

public class MainActivityObserver extends BaseObserver {

    Lifecycle mLifecycle;
    Context mContext;
    UICallback mCallback;


    public MainActivityObserver(Context context, Lifecycle lifecycle, UICallback callback) {
        BaseObserver(context, lifecycle, callback);
    }

    @Override
    public void BaseObserver(Context context, Lifecycle lifecycle, UICallback callback) {
        mLifecycle = lifecycle;
        mContext = context;
        mCallback = callback;
        mLifecycle.addObserver(this);
    }

    @Override
    public void onViewCreated() {
    }

    @Override
    public void onViewStarted() {

    }

    @Override
    public void onViewStopped() {

    }

    @Override
    public void onViewResumed() {
        setText();
    }

    @Override
    public void onViewDestroyed() {
        mLifecycle.removeObserver(this);
    }

    @Override
    public void onViewPaused() {

    }

    public void setText(){
        if(mLifecycle.getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
        }
    }
}
