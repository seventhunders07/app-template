package ph.seventhunders.apptemplate.database;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;

import io.objectbox.Box;
import io.objectbox.android.ObjectBoxLiveData;
import ph.seventhunders.apptemplate.BaseApplication;
import ph.seventhunders.apptemplate.model.User;
import ph.seventhunders.apptemplate.model.User_;

/**
 * Created by root on 2/28/18.
 */

public class BoxDao<T> {

    public BoxDao() {
    }

    public Box<T> getDataEntity(Class<T> parsingClass) {
        return BaseApplication.getInstance().mBoxStore.boxFor(parsingClass);
    }

    public void insertAllData(Class<T> entityClass, ArrayList<T> data) {
        getDataEntity(entityClass).put(data);
    }

    public void insertData(Class<T> entityClass, T data) {
        long id = getDataEntity(entityClass).put(data);
        Log.d("BoxDao", "insertData: " + id);
    }

    public void updateData(Class<T> entityClass, T data, int index) {
        getDataEntity(entityClass).put(data);
    }

    public ArrayList<T> getAllData(Class<T> entityClass) {
        return new ArrayList<>(getDataEntity(entityClass).query().build().findLazyCached());
    }

    public ObjectBoxLiveData<T> getUserLiveData(Class<T> entityClass){
        return new ObjectBoxLiveData<>(getDataEntity(entityClass).query().build());
    }

}
