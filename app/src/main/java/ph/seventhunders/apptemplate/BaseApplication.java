package ph.seventhunders.apptemplate;

import android.app.Application;

import io.objectbox.BoxStore;
import io.objectbox.DebugFlags;
import ph.seventhunders.apptemplate.model.MyObjectBox;

/**
 * Created by root on 2/28/18.
 */

public class BaseApplication extends Application {

    private static BaseApplication INSTANCE;
    public BoxStore mBoxStore;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        initializeObjectBox();
    }

    private void initializeObjectBox(){
        mBoxStore = MyObjectBox.builder().androidContext(this).debugTransactions().build();
    }

    public static BaseApplication getInstance(){
        return INSTANCE;
    }
}
